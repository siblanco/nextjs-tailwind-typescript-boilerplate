import { useEffect } from 'react';

interface ILayoutProps {
	children: React.ReactNode;
}

const Layout = ({ children }: ILayoutProps) => {
	useEffect(() => {
		Array(children).length ? console.log('yo') : console.log('no');
	}, [children]);
	return <div>{children}</div>;
};

export default Layout;
